module _startup;

import ldc.attributes;

extern (C):

extern const void* _estack, _sidata, _sdata, _edata, _sbss, _ebss;

void main();

@noreturn void irq_reset_handler()
{
    void** data_in_flash, ram;

    for (data_in_flash = cast(void**)&_sidata, ram = cast(void**)&_sdata; ram != &_edata;
        data_in_flash++, ram++)
    {
        *ram = *data_in_flash;
    }

    for (ram = cast(void**)&_sbss; ram != &_ebss; ram++)
    {
        *ram = null;
    }
    main();
    while (1)
    {
    }
}

@noreturn void irq_default_handler()
{
    while (1)
    {
    }
}

@weak alias irq_systick_handler = irq_default_handler;
static foreach (num; ["0", "1", "2", "3", "4", "5_9", "10_15"])
{
    mixin("@weak alias irq_exti" ~ num ~ "_handler = irq_default_handler;");
}

@section(".isr_vector") void*[16] vectors = [
    cast(void*)&_estack, cast(void*)&irq_reset_handler, null, null, null, null,
    null, null, null, null, null, cast(void*)&irq_exti0_handler,
    cast(void*)&irq_exti1_handler, cast(void*)&irq_exti2_handler,
    cast(void*)&irq_exti3_handler, cast(void*)&irq_exti4_handler,
];
