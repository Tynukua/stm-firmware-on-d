module hal.rcc;

import ldc.attributes;

import hal.common;

extern (C):

enum HAL_SYSCLK_SRC_HSI = 0b00;
enum HAL_SYSCLK_SRC_HSE = 0b01;
enum HAL_SYSCLK_SRC_PLL = 0b10;

// _HAL_RCC_H_

struct RCCBase
{
    enum rccBase = cast(RCCBase*)(RCC_BASE_ADDR);
    uint ctrl;
    uint cfg;
    uint ci;
    uint apb2rst;
    uint apb1rst;
    uint ahben;
    uint apb2en;
    uint apb1en;
    uint bdc;
    uint cs;

    static void sysclk_source(int src)
    {
        rccBase.cfg &= ~0b11;
        rccBase.cfg |= src & 0b11;
        while ((rccBase.cfg & 0b1100) != (src & 0b11) << 2)
        {
        }
    }
}

struct RCCAlias
{
    enum rccAlias = cast(RCCAlias*) PERIF_ALIAS_ADDR!RCC_BASE_ADDR;
    uint[32] ctrl;
    uint[32] cfg;
    uint[32] ci;
    uint[32] apb2rst;
    uint[32] apb1rst;
    uint[32] ahben;
    uint[32] apb2en;
    uint[32] apb1en;
    uint[32] bdc;
    uint[32] cs;

    static void disableGPIO(uint port)
    {
        rccAlias.apb2en[port + 2] = 0;
    }

    static void enableGPIO(uint port)
    {
        rccAlias.apb2en[port + 2] = 1;
    }

    static auto isGPIOEnabled(uint port)
    {
        return rccAlias.apb2en[port + 2];
    }

    static void enableAFIO()
    {
        rccAlias.apb2en[0] = 1;
    }

    static void disableAFIO()
    {
        rccAlias.apb2en[0] = 0;
    }

    static int isAFIOEnabled()
    {
        return rccAlias.apb2en[0];
    }

}
