module hal.systick;

import ldc.attributes;

import hal.common;

extern (C):

enum HAL_SYSTICK_SRC
{
    AHB_DIV_8 = 0x0,
    AHB = 0x1
}

// _HAL_SYSTICK_H_

struct SystickBase
{
    enum systickBase = cast(SystickBase*) SYSTICK_BASE_ADDR;

    uint ctrl;
    uint reload;
    uint current;
    uint calib;

    static void init(ubyte intrEnable, uint source, uint value)
    {
        set(value);
        if (intrEnable)
        {
            systickBase.ctrl |= 0b10;
        }
        systickBase.ctrl |= source << 2;
    }

    static void enable()
    {
        systickBase.ctrl |= 1;
    }

    static void disable()
    {
        systickBase.ctrl &= ~1;
    }

    static void set(uint value)
    {
        systickBase.reload = value & 0xFFFFFF;
        restart();
    }

    static void restart()
    {
        systickBase.current = 0;
    }

    static uint get()
    {
        return systickBase.current;
    }
}
