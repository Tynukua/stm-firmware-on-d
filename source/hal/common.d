module hal.common;

extern (C):

enum PERIF_BITBAND
{
    REGION = 0x40000000,
    ALIAS = 0x42000000,
}

enum PERIF_ALIAS_ADDR(size_t perif_addr) = PERIF_BITBAND.ALIAS + (
        perif_addr - PERIF_BITBAND.REGION) * 0x20;

pragma(inline, true) auto perifAliasAddr(size_t addr)
{
    return PERIF_BITBAND.ALIAS + (addr - PERIF_BITBAND.REGION) * 0x20;
}

enum RCC_BASE_ADDR = 0x40021000;

enum GPIO_BASE_ADDR = 0x40010800;
enum GPIO_MODE_EXTI = 0b10000;
enum GPIO_OFFSET = 0x400;
enum GPIO_MODE_INP_FL = 0b0100;
enum AFIO_BASE_ADDR = 0x40010000;
enum NVIC_BASE_ADDR = 0xE000E100;
enum SYSTICK_BASE_ADDR = 0xE000E010;

// void __aeabi_unwind_cpp_pr0()
// {
// }
// 
// void __aeabi_memcpy4(ubyte* dest, const ubyte* src, size_t n)
// {
// foreach (i; 0 .. n)
// {
// dest[i] = src[i];
// }
// }
// 
// void __aeabi_memclr(ubyte* dest, size_t n)
// {
// foreach (i; 0 .. n)
// {
// dest[i] = 0;
// }
// }
// 
// void __aeabi_memclr4(ubyte* dest, size_t n)
// {
// __aeabi_memclr(dest, n);
// }
// 
// void __aeabi_read_tp()
// {
// }
