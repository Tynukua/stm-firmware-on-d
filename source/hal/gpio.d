module hal.gpio;
import hal.common;

struct GPIO
{
    private GPIOBase* _base;
    private GPIOAlias* _alias;
    uint pin;

    this(uint port, uint pin, uint mode)
    {
        this.pin = pin;
        _base = cast(GPIOBase*)(GPIO_BASE_ADDR + GPIO_OFFSET * port);
        _alias = cast(GPIOAlias*) perifAliasAddr(GPIO_BASE_ADDR + GPIO_OFFSET * port);
        if (mode & GPIO_MODE_EXTI)
        {
            uint exti_reg = pin / 4;
            uint line_pos = (pin - exti_reg * 4) * 4;
            AFIOBase.addr.exti[exti_reg] &= ~(0b1111 << line_pos);
            AFIOBase.addr.exti[exti_reg] |= port << line_pos;
        }
        uint moder_pos = (pin < 8 ? pin : pin - 8) * 4;
        uint* cfg_reg = (pin < 8 ? &_base.crl : &_base.crh);

        *cfg_reg &= ~(0b1111 << moder_pos);

        *cfg_reg |= mode << moder_pos;
    }

    uint read()
    {
        return _alias.od[pin];
    }

    void write(uint v)
    {
        _alias.od[pin] = cast(bool) v;
    }

    void switch_()
    {
        write(!read);
    }

    void destroy()
    {
        uint exti_reg = pin / 4;
        uint line_pos = (pin - exti_reg * 4) * 4;
        uint moder_pos = (pin < 8 ? pin : pin - 8) * 4;
        uint* cfg_reg = (pin < 8 ? &_base.crl : &_base.crh);
        *cfg_reg &= ~(0b1111 << moder_pos);
        *cfg_reg |= GPIO_MODE_INP_FL << moder_pos;
        AFIOBase.addr.exti[exti_reg] &= ~(0b1111 << line_pos);

    }

    enum Port
    {
        A,
        B,
        C,
        D,
        E,
        F,
        G,
    }

    enum Mode
    {
        SPEED_LOW = 0b0010,
        SPEED_MID = 0b0001,
        SPEED_HIGH = 0b0011,

        OUT_PP = 0b0000,
        OUT_OD = 0b0100,
        ALT_PP = 0b1000,
        ALT_OD = 0b1100,
        INP_AG = 0b0000,
        INP_FL = 0b0100,
        INP_PP = 0b1000,

        EXTI = 0b10000,
    }

}

struct GPIOBase
{
    uint crl;
    uint crh;
    uint id;
    uint od;
    uint bsr;
    uint br;
    uint lck;
}

struct GPIOAlias
{
    struct Moder
    {
        uint[2] mode;
        uint[2] cnf;
    }

    Moder[16] moder;
    uint[32] id;
    uint[32] od;
    uint[32] bsr;
    uint[32] br;
    uint[32] lck;
}

struct AFIOBase
{
    enum addr = cast(AFIOBase*) AFIO_BASE_ADDR;
    alias addr this;

    uint evc;
    uint map1;
    uint[4] exti;
    uint map2;
}
