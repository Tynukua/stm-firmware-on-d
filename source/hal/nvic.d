module hal.nvic;
import hal.common;

struct NVICBase
{
    enum addr = cast(NVICBase*) NVIC_BASE_ADDR;
    uint[8] ise;
    uint[24] _f;
    uint[8] ice;

    static void enable_irq(uint irqn)
    {
        uint ise_idx = irqn / 32;
        uint irq_pos = irqn - ise_idx * 32;

        addr.ise[ise_idx] |= 1 << irq_pos;
    }

    static void disable_irq(uint irqn)
    {
        uint ice_idx = irqn / 32;
        uint irq_pos = irqn - ice_idx * 32;

        addr.ice[ice_idx] |= 1 << irq_pos;
    }

    static int enabled(uint irqn)
    {
        uint ise_idx = irqn / 32;
        uint irq_pos = irqn - ise_idx * 32;

        return addr.ise[ise_idx] & (1 << irq_pos);
    }
}
