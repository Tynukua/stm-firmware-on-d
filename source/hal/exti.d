module hal.exti;
import hal.common;
import hal.nvic;

enum EXTI_BASE_ADDR = 0x40010400;

enum EXTI_EDGE_RISING = 0b01;
enum EXTI_EDGE_FALLING = 0b10;

enum NVIC_IRQN_LINE0_4 = 6;
enum NVIC_IRQN_LINE5_9 = 23;
enum NVIC_IRQN_LINE10_15 = 40;

struct EXTIAlias
{
    enum addr = cast(EXTIAlias*) PERIF_ALIAS_ADDR!EXTI_BASE_ADDR;
    uint[32] im;
    uint[32] em;
    uint[32] rts;
    uint[32] fts;
    uint[32] swie;
    uint[32] pnd;

    static void enable_interrupt(uint line, uint trigger_edge)
    {
        if (trigger_edge & EXTI_EDGE_RISING)
            addr.rts[line] = 1;
        if (trigger_edge & EXTI_EDGE_FALLING)
            addr.fts[line] = 1;

        NVICBase.enable_irq(line.ltoirqn);
        addr.im[line] = 1;
    }

    static void disable_interrupt(uint line)
    {
        addr.im[line] = addr.rts[line] = addr.fts[line] = 0;
    }

    static auto get_pending(uint line)
    {
        return addr.pnd[line];
    }

    static void clear_pending(uint line)
    {
        addr.pnd[line] = 0;
    }

}

static private int ltoirqn(uint line)
{
    if (line <= 4)
        return line + NVIC_IRQN_LINE0_4;
    else if (line <= 9)
        return line + NVIC_IRQN_LINE5_9;
    return NVIC_IRQN_LINE10_15;
}
