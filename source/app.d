import ldc.attributes;

import hal.systick;
import hal.rcc;
import hal.gpio;
import hal.exti;

extern (C):

__gshared GPIO bt;
__gshared GPIO led;
void main()
{
    RCCBase.sysclk_source = HAL_SYSCLK_SRC_HSI;
    RCCAlias.enableGPIO = GPIO.Port.A;
    RCCAlias.enableGPIO = GPIO.Port.B;
    RCCAlias.enableAFIO;
    bt = GPIO(GPIO.Port.A, 2, GPIO.Mode.INP_PP | GPIO.Mode.EXTI);
    led = GPIO(GPIO.Port.B, 10, GPIO.Mode.OUT_PP | GPIO.Mode.SPEED_LOW);
    led.write = 0;
    bt.write = 0;
    EXTIAlias.enable_interrupt(2, EXTI_EDGE_RISING);

    while (1)
    {
    }
}

void irq_exti2_handler()
{
    for (int i = 0; i < 1000; ++i)
    {
    }

    if (bt.read)
        led.switch_;
    EXTIAlias.clear_pending(2);
}
