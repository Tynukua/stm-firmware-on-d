from alpine

workdir /opt/firmware

run apk update 

run apk add dub
run apk add ldc
run apk add gcc-arm-none-eabi

COPY source source
COPY dub.sdl ./
COPY main.ld ./

run dub build

from alpine
workdir /opt/firmware
COPY --from=0 /opt/firmware/a.bin ./
cmd ls -l1
