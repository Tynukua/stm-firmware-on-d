import json
import os


class PeripheralConfig:
    def __init__(self):
        self.perifs = {}

    def get_addr(self, name):
        if name in self.perifs:
            return self.perifs[name]
        else:
            return -1

    def add_reg(self, name, base, offset):
        reg_addr = int(base, 0) + int(offset, 0)
        self.perifs[name] = reg_addr


class TestConfig:
    def __init__(self, description, reg):
        self.description = description
        self.reg = reg
        self.bitmask = 0
        self.bit_off = 0
        self.test_value = 0

    def calc_bitmask(self, bitcount):
        self.bitmask = pow(2, bitcount) - 1

    def set_bit_offset(self, off):
        self.bit_off = off

    def set_test_value(self, value):
        self.test_value = value


class TestsConfig:
    def __init__(self):
        self.mcu = ""
        self.perif = PeripheralConfig()
        self.tests = []
        self.sleep_before = 0

    def set_mcu(self, name):
        self.mcu = name

    def set_sleep_before(self, secs):
        self.sleep_before = secs

    def add_test(self, test):
        self.tests.append(test)

    def get_perif(self):
        return self.perif

    def get_mcu(self):
        return self.mcu

    def get_sleep_before(self):
        return self.sleep_before

    def get_tests(self):
        return self.tests


class ConfigReader:
    def __init__(self, config_path):
        self.test_config = TestsConfig()
        self._parse(self._read_config(config_path), config_path)

    def get_config(self):
        return self.test_config

    def _read_config(self, config_path):
        content = ""
        with open(config_path, "r") as conf:
            content = conf.read()
        return content

    def _parse(self, content, config_path):
        config = json.loads(content)
        for fld in config.keys():
            if fld == "mcu":
                self.test_config.set_mcu(config[fld])
            elif fld == "sleep_before":
                self.test_config.set_sleep_before(config[fld])
            elif fld == "perif":
                self._parse_perifs(config[fld])
            elif fld == "tests":
                self._parse_tests(config[fld])
            elif fld == "includes":
                for inc in config[fld]:
                    cfg_path = os.path.join(os.path.dirname(config_path), inc)
                    cnt = self._read_config(cfg_path)
                    self._parse(cnt, cfg_path)

    def _parse_perifs(self, perifs_node):
        for perif in perifs_node.keys():
            base = perifs_node[perif]["base"]
            for reg in perifs_node[perif]["regs"]:
                reg_name = reg[0]
                reg_off = reg[1]
                self.test_config.get_perif().add_reg(
                    f"{perif}_{reg_name}", base, reg_off)

    def _parse_tests(self, tests_node):
        for test in tests_node:
            t = TestConfig(test["description"], test["reg"])
            t.set_test_value(int(test["value"], 0))
            bits = test["bits"]
            low = high = 0
            if len(bits) == 1:
                low = high = bits[0]
            else:
                low = bits[0]
                high = bits[1]
            t.calc_bitmask(high - low + 1)
            t.set_bit_offset(low)
            self.test_config.add_test(t)
