from tester.ConfigReader import TestsConfig
import subprocess
from time import sleep


class TestResult:
    def __init__(self, description, result, test_value):
        self.description = description
        self.result = result
        self.test_value = test_value

    def get_result(self):
        return self.result == self.test_value


class Tester:
    def __init__(self, gdb, firmware, test_config: TestsConfig):
        self.gdb = gdb
        self.firmware = firmware
        self.tests_config = test_config
        self.results = []

    def run_tests(self):
        emu = self.run_emulator()
        print("==== TEST START ====")
        print(f"Wait {self.tests_config.get_sleep_before()} seconds...")
        sleep(self.tests_config.get_sleep_before())
        for test in self.tests_config.get_tests():
            res = self.select_value(test.reg, test.bitmask, test.bit_off)
            r = TestResult(test.description, res, test.test_value)
            self.results.append(r)
        emu.kill()

    def get_results(self):
        successfull_counter = 0
        for res in self.results:
            if res.get_result():
                print(f'[OK]\t{res.description}')
                successfull_counter += 1
            else:
                print(f'[FAIL]\t{res.description}')
        print(f'Successfull: {successfull_counter}/{len(self.results)}')

    def run_emulator(self):
        process = subprocess.Popen([
            'qemu-system-gnuarmeclipse',
            '--mcu', self.tests_config.get_mcu(),
            '--image', self.firmware,
            '--nographic',
            '--gdb', 'tcp::3333'
        ], stdout=subprocess.PIPE)
        sleep(1)
        return process

    def read_addr(self, addr):
        process = subprocess.Popen([
            f'{self.gdb}',
            '-q', f'{self.firmware}',
            '-ex', 'target remote :3333',
            '-ex', f'x/1xw {addr}',
            '-ex', 'quit'
        ], stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        process.kill()
        result = str(stdout)
        s = result.find(addr) + len(addr) + 3
        return int(result[s:s+10], 16)

    def select_value(self, reg, bitmask, offset):
        addr = self.tests_config.get_perif().get_addr(reg)
        value = 0
        if addr >= 0:
            value = self.read_addr(hex(addr))
        return (value & (bitmask << offset)) >> offset
